import pandas as pd

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titanic_dataframe()

    # Define the list of titles to consider
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]

    
    results = []

    
    for title in titles_to_consider:
        filtered_data = df[df['Name'].str.contains(title)]
        missing_values = filtered_data['Age'].isnull().sum()
        median_age = filtered_data['Age'].median()
        results.append((title, missing_values, median_age))

    return results
